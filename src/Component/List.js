import {Button, List,} from 'antd';
import {DeleteOutlined, ExclamationOutlined} from '@ant-design/icons';
import {useState} from "react";

export default function Lists(props) {

    const [category, setCategory] = useState('a')

    const {data, deleteTodo, setStatus,getStatus} = props

    const deleteItem = (id) => {
        deleteTodo(id)
    }

    const setDone = (id, done) => {
        setStatus(id, done)
    }

    const handleChangeCategory = (value) => {
        setCategory(value);
        getStatus(value);
    }


    return (
        <div className="mt-3 container">
            <div className='row'>
                <div className="col-lg-4 offset-lg-4">
                    <Button
                        onClick={() => handleChangeCategory('a')}
                        type={`${category === 'a' ? 'primary' : 'default'}`}
                    >All</Button>
                    <Button
                        onClick={() => handleChangeCategory('b')}
                        type={`${category === 'b' ? 'primary' : 'default'}`}
                    >Active</Button>
                    <Button
                        onClick={() => handleChangeCategory('c')}
                        type={`${category === 'c' ? 'primary' : 'default'}`}
                    >Done</Button>


                    <div className="mt-2"></div>

                    <List
                        size="small"
                        bordered
                        dataSource={data}
                        renderItem={item =>

                            <List.Item>
                                {item.done ? <b><u>{item.item}</u></b> : <span>{item.item}</span>}
                                <span>
                                    <Button

                                        danger
                                        icon={<DeleteOutlined/>}
                                        style={{marginRight: "10px"}}
                                        onClick={() => deleteItem(item.id)}
                                    />
                                    <Button
                                        icon={<ExclamationOutlined/>}

                                        onClick={() => setDone(item.id, item.done)}
                                    />
                                </span>
                            </List.Item>
                        }
                    />
                </div>
            </div>
        </div>
    )
}