import {Button, Input} from "antd";
import {useState} from 'react';

export default function Add(props) {

    const {addTodo}=props

    const [input, setInput] = useState('')

    const addItem = () => {
        addTodo(input)
    }

    return (
        <div className="mt-3 container">
            <div className='row'>
                <div className='col-lg-4 offset-lg-4'>
                    <div className="d-flex">
                        <Input
                            placeholder="Add todo"
                            value={input}
                            onChange={e => setInput(e.target.value)}
                        />
                        <Button onClick={addItem}>Add</Button>
                    </div>
                </div>
            </div>
        </div>
    )
}