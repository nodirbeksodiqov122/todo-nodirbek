import {Button, Input} from 'antd';
import {useState} from "react";

export default function Todo(props) {

    const [input, setInput] = useState('')

    const {filter, done, list} = props;

    const search = () => {
        filter(input)
    }


    return (
        <div className='container mt-4'>
            <div className="row">
                <div className="col-lg-4 offset-lg-4">
                    <div className="d-flex justify-content-between">
                        <h3>To do list </h3>
                        <h5>{list} more to do, {done} done</h5>
                    </div>
                    <div className="d-flex">
                        <Input
                            type='search'
                            placeholder="Search add todo"
                            value={input}
                            onChange={e => setInput(e.target.value)}
                        />
                        <Button onClick={search}>Search</Button>
                    </div>

                </div>
            </div>
        </div>

    )
}