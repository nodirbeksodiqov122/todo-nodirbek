import * as actionTypes from './actionTypes'

export const todoStart = () => {
    return {
        type: actionTypes.TODO_START
    }
}

export const todoSuccess = (todoList) => {
    return {
        todoList: todoList,
        type: actionTypes.TODO_SUCCESS
    }
}


export const fetchTodo = (todoList) => {
    return dispatch => {
        dispatch(todoStart());
        dispatch(todoSuccess(todoList))
    }
}
