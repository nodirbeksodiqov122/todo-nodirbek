import * as actionTypes from '../actions/actionTypes'
import {updateObject} from '../utility'


const initialState = {
    todoList: [],
    loading: false,
}


const todoStart = (state) => {
    return updateObject(state, {
        loading: true,
    })
}

const todoSuccess = (state, action) => {

    return updateObject(state, {
        todoList: action.todoList,
        loading: false,
    })
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.TODO_START:
            return todoStart(state)
        case actionTypes.TODO_SUCCESS:
            return todoSuccess(state,action)
        default:
            return state
    }
}

export default reducer;


