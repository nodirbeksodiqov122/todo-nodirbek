import {BrowserRouter as Router, Route, Switch,} from "react-router-dom";
import Todo from "./Component/Todo";
import Lists from "./Component/List";
import Add from "./Component/Add";
import {fetchTodo} from "./store/actions/todo";
import {useEffect, useState} from "react";
import {connect} from "react-redux";

function App(props) {

    const [type, setType] = useState(null)
    const [todos, setTodos] = useState([])
    const [done, setDone] = useState(0)
    const [list, setList] = useState(2)
    const [allFilter, setAllFilter] = useState(null)
    const [getTodo, setGetTodo] = useState([])

    const {todoList} = props

    useEffect(() => {
        const list = [
            {
                id: 73753575,
                item: '1',
                done: false
            },
            {
                id: 725752527,
                item: '1234',
                done: false
            }
        ]
        props.fetchTodo(list)
        count()
    }, [])


    const filter = (title) => {


        setAllFilter('yes')
        let patt1 = RegExp(title, "gi");
        const arrayFilter = todoList.filter(item => item.item.match(patt1))
        setGetTodo(arrayFilter)


    }


    const addTodo = (input) => {
        todoList.push(
            {
                id: Date.now(),
                item: input,
                done: false
            }
        )
        props.fetchTodo(todoList)
        getStatus(type)
        count()
    }

    const deleteTodo = (id) => {
        let objectId = todoList.findIndex(obj => obj.id === id)
        todoList.splice(objectId, 1)
        props.fetchTodo(todoList)
        getStatus(type)
        count()

    }

    const setStatus = (id, done) => {

        let objectId = todoList.findIndex(obj => obj.id === id)

        if (done === true) {
            todoList[objectId].done = false
            props.fetchTodo(todoList)
        } else {
            todoList[objectId].done = true
            props.fetchTodo(todoList)
        }
        getStatus(type)
        count()

    }

    const getStatus = (status) => {
        if (status){
            setType(status)

            setAllFilter('no')

            let todos = []

            if (status === 'a') {
                todos = todoList
            } else if (status === 'b') {
                todoList.map((item) => !item.done ? todos.push(item) : null)
            } else if (status === 'c') {
                todoList.map((item) => item.done ? todos.push(item) : null)

            }
            setTodos(todos)
            count()
        }

    }

    const count = () => {
        let counter = []
        todoList.map((item) => item.done ? counter.push(item) : null)
        setDone(counter.length)
        setList(todoList.length)
    }

    return (
        <div className="App">
            <Router>
                <Switch>
                    <Route exact path="/">
                        <Todo
                            list={list}
                            done={done}
                            filter={(title) => filter(title)}
                        />
                        <Lists
                            data={allFilter === 'no' ? todos
                                : allFilter === 'yes' ? getTodo : todoList}
                            deleteTodo={(id) => deleteTodo(id)}
                            setStatus={(id, done) => setStatus(id, done)}
                            getStatus={(status) => getStatus(status)}
                        />
                        <Add
                            addTodo={(input) => addTodo(input)}
                        />
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        todoList: state.todo.todoList,
        loading: state.todo.loading,
    };
};


const mapDispatchToProps = dispatch => {
    return {
        fetchTodo: (todoList) => dispatch(fetchTodo(todoList))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);

